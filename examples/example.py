import sys, os
print(sys.path)

from NXOpen import Session, BasePart

def print_modules():
    for module in (filter(lambda x: x.startswith("NXOpen"), sys.modules.keys())):
        print (module)

theSession = Session.GetSession()
lw = theSession.ListingWindow
lw.Open()
lw.WriteLine("Hello World!")

root = os.path.dirname(__file__)
testfile = os.path.abspath(os.path.join(root, "example.prt"))
workpart: BasePart
workpart, _ = theSession.Parts.Open(testfile)

print(workpart.Name)

builder = workpart.Features.CreateBlockFeatureBuilder(None)

builder.Commit()
builder.Destroy()
