@rem Evironment settings required for to run NX Headless from a python shell

@echo OFF
set UGII_BASE_DIR=C:\Program Files\Siemens\NX1899
echo %UGII_BASE_DIR%
set base="%UGII_BASE_DIR%"
set base_dir="%UGII_BASE_DIR%"
set DISPLAY="LOCALPC:0.0"
set PATH=%UGII_BASE_DIR%\nxbin;%UGII_BASE_DIR%\ugii;%PATH%

rem NXOpen Python Variables
set PYTHONPATH=%UGII_BASE_DIR%\NXBIN\python

rem Start PowerShell (Core)
rem powershell
REM pwsh
rem CMD
cmd

rem Example NX Headless mit Python nutzen
rem python
rem from NXOpen import Session
rem the_session = Session.GetSession()
rem workpart = the_session.Parts.Work