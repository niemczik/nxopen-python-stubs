import sys
import subprocess
from os import path, environ
import setuptools
from wheel.bdist_wheel import bdist_wheel
from distutils import log
from helpers.nx_version import NXVersionInfo

root = path.dirname(__file__)
class BuildStubsExtension(bdist_wheel):
    user_options = bdist_wheel.user_options + [
        ('ugii-base-dir=', None, 'NX build_extation used to build stubs from/for.')
    ]

    def initialize_options(self):
        super().initialize_options()
        self.ugii_base_dir = environ.get("UGII_BASE_DIR", None)
        if not self.ugii_base_dir:
            raise Exception("'UGII_BASE_DIR' environment variable not found. " + \
            "Make sure NX is installed and 'UGII_BASE_DIR' points to the NX installation folder.")

    def finalize_options(self):
        super().finalize_options()
        environ["UGII_BASE_DIR"] = self.ugii_base_dir
        environ["PYTHONPATH"] = path.join(environ["UGII_BASE_DIR"], "NXBIN", "python")

    def run(self):
        nx_version = NXVersionInfo().get_major_version()
        self.announce("Try to install build dependencies...")
        self.try_install_requirements()
        self.announce(f"Building NX Open {nx_version} stubs...", log.INFO)
        self.build_stubs()
        self.distribution.metadata.version += "-NX" + nx_version.lstrip("V")
        # Include stubs after building
        self.distribution.packages = setuptools.find_packages(exclude=["helpers", "examples"])
        super().run()

    def try_install_requirements(self):
        try:
            requirements = f'"{path.join(root, "requirements.txt")}"'
            subprocess.check_call([sys.executable, "-m", "pip", "install", "-r", requirements])
        except Exception as e:
            self.announce(f"Optional dependencies couldn't be installed, continue anyway. Error: {e}", log.ERROR)

    def build_stubs(self):
        root = path.dirname(__file__)
        build_script = path.join(root, "build_stubs.py")
        subprocess.run("python " + build_script)

print("Install NXOpen Stubs")

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    cmdclass={
        "bdist_wheel": BuildStubsExtension,
    },
    name="NXOpen Python Stubs", # Replace with your own username
    version="0.9",
    author="Lars Niemczik",
    author_email="niemczik@dik.tu-darmstadt.de",
    description="Python stubs for NX Open",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    package_data={
      "": ["*.pyi"],
    },
    include_package_data = True,
    classifiers=[
        "Development Status :: 2 - Pre-Alpha"
        "Programming Language :: Python :: 3.7",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
)