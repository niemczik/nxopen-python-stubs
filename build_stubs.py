import sys, os
from os import path, makedirs
import logging as log

import argparse

# Argparsing

parser = argparse.ArgumentParser(description='Generate python stubfiles for NXOpen')
parser.add_argument("--out-dir", type=str, default=None, help="Specifiy another location for the output. Defaults to the location of this script.")

args = parser.parse_args()

root = path.dirname(__file__)

out_dir = root if args.out_dir is None else path.abspath(args.out_dir)

ugii_base_dir = os.environ.get("UGII_BASE_DIR", None)
# # NX Environment Variables
os.environ["base"] = ugii_base_dir
os.environ["base_dir"] = ugii_base_dir
os.environ["PATH"] = f"{ugii_base_dir}/nxbin;{ugii_base_dir}/ugii;{os.environ['PATH']}"

addin_docs_path = path.join(root, "addin_docs.json")

indent = 4*" "

default_imports = [
    "from __future__ import annotations",
    "from typing import List, Tuple",
    "from enum import Enum",
    "",
]

extension = ".pyi"
init_extension = ".py"

import json
from helpers.nx_version import NXVersionInfo

DEFAULT_MODULES = [
    "NXOpen",
    "NXOpen.AnimationDesigner",
    "NXOpen.AnimationDesigner.Nav",
    "NXOpen.Annotations",
    "NXOpen.Assemblies",
    "NXOpen.Assemblies.ProductInterface",
    "NXOpen.BlockStyler",
    "NXOpen.BodyDes",
    "NXOpen.CAE",
    "NXOpen.CAE.AeroStructures",
    "NXOpen.CAE.AeroStructures.Author",
    "NXOpen.CAE.Connections",
    "NXOpen.CAE.FTK",
    "NXOpen.CAE.ModelCheck",
    "NXOpen.CAE.Optimization",
    "NXOpen.CAE.QualityAudit",
    "NXOpen.CAE.ResponseSimulation",
    "NXOpen.CAE.Xyplot",
    "NXOpen.CAM",
    "NXOpen.CAM.FBM",
    "NXOpen.CLDKin",
    "NXOpen.Diagramming",
    "NXOpen.Diagramming.Tables",
    "NXOpen.DiagrammingLibraryAuthor",
    "NXOpen.Die",
    "NXOpen.Display",
    "NXOpen.DMU",
    "NXOpen.Drafting",
    "NXOpen.Drawings",
    "NXOpen.ElectricalRouting",
    "NXOpen.Facet",
    "NXOpen.Features",
    "NXOpen.Features.SheetMetal",
    "NXOpen.Features.ShipDesign",
    "NXOpen.Features.ShipDesign.GeneralArrangement",
    "NXOpen.Features.Subdivision",
    "NXOpen.Features.VehicleDesign",
    "NXOpen.Fields",
    "NXOpen.Formboard",
    "NXOpen.Gateway",
    "NXOpen.GeometricAnalysis",
    "NXOpen.GeometricAnalysis.SectionAnalysis",
    "NXOpen.GeometricUtilities",
    "NXOpen.GeometricUtilities.UVMapping",
    "NXOpen.Issue",
    "NXOpen.Join",
    "NXOpen.Layer",
    "NXOpen.Layout2d",
    "NXOpen.LineDesigner",
    "NXOpen.Markup",
    "NXOpen.MechanicalRouting",
    "NXOpen.Mechatronics",
    "NXOpen.MenuBar",
    "NXOpen.Mfg",
    "NXOpen.Mfg.AM",
    "NXOpen.Mfg.Mlp",
    "NXOpen.MfgModel",
    "NXOpen.MFGViewMaker",
    "NXOpen.ModlDirect",
    "NXOpen.ModlUtils",
    "NXOpen.Motion",
    "NXOpen.OpenXml",
    "NXOpen.Optimization",
    "NXOpen.Options",
    "NXOpen.PartFamily",
    "NXOpen.PDM",
    "NXOpen.PDM.SaveManagement",
    "NXOpen.PhysMat",
    "NXOpen.PID",
    "NXOpen.Placement",
    "NXOpen.PLAS",
    "NXOpen.Positioning",
    "NXOpen.Preferences",
    "NXOpen.PressLineSimulation",
    "NXOpen.Report",
    "NXOpen.Routing",
    "NXOpen.Routing.Electrical",
    "NXOpen.ShapeSearch",
    "NXOpen.SheetMetal",
    "NXOpen.ShipDesign",
    "NXOpen.SIM",
    "NXOpen.SIM.PostConfigurator",
    "NXOpen.TDP",
    "NXOpen.ToolDesigner",
    "NXOpen.Tooling",
    "NXOpen.UF",
    "NXOpen.UIStyler",
    "NXOpen.UserDefinedObjects",
    # "NXOpen.UserDefinedTemplate",
    "NXOpen.Validate",
    "NXOpen.VisualReporting",
    "NXOpen.Weld",
]

version_info = NXVersionInfo()
version_number = version_info.get_major_version().lstrip("V")

with open(os.path.join(root, "doc_urls.json"), "r") as f:
    doc_urls = json.loads(f.read())

def get_available_modules_from_doc(doc_url):
    from requests import request
    from bs4 import BeautifulSoup

    response = request("GET", doc_url)
    if response.status_code != 200:
        raise Exception(f"Error loading '{doc_url}'. Status code {response.status_code}")
    html = BeautifulSoup(response.content, "html.parser")
    table_element = html.find(attrs={"id": "row_0_"}).parent
    doc_modules = []
    branch = []
    for sub_module in table_element.find_all("tr"):
        level = sub_module.attrs["id"].count("_")-2
        sub_module = sub_module.find("a").text
        if level > 0:
            branch = branch[:level]
        branch.append(sub_module)
        doc_modules.append(".".join(branch))
    return doc_modules

try:
    modules_to_load = get_available_modules_from_doc(doc_urls[version_number])
except Exception as e:
    modules_to_load = DEFAULT_MODULES
    log.error(f"Error during namespace evaluation, using default modules. Error: {e}")


# Application
import importlib
import inspect

from helpers.stubs import *

qualname_regex = re.compile(r"<class '(.*?)'>")
def generate_class(cls, module=""):
    doc = format_doc(cls.__doc__)
    is_enum = is_enum_class(doc)
    
    if is_enum:
        cls_def = EnumDefnition()
    else:
        cls_def = ClassDefinition()
    
    cls_def.name = cls.__name__
    cls_def.doc = doc

    base_import_types = []

    for base_cls in cls.__bases__:
        qualname = qualname_regex.search(str(base_cls)).group(1)
        base_import_types.append(qualname)
        alias = generate_alias(qualname)
        cls_def.base_classes.append(alias)

    if(is_enum):
        cls_def = generate_enum_members(cls_def, module, cls.__name__)

    cls_def.members = generate_members(cls, module, cls.__name__)
    # cls_def.members = []

    return cls_def

enum_class = re.compile(r'Enum Members')
# enum_members = re.compile(r'(:header:.*?\n)(\s+\"((.|\n)*?))(\n(\s){0,}\n)')
enum_members = re.compile(r'(?::header:.*?\n)(\s+((.|\n)*))')
# enum_values = re.compile(r'^(\s{2,}\")(\w+)(\")', flags=RegexFlag.MULTILINE)
enum_values = re.compile(r'"(\w+)".*?"((\w|\s)+)"')
def is_enum_class(doc):
    return not enum_class.search(doc) is None

def generate_enum_members(enum_def, module ="", cls=""):
    members = enum_members.search(enum_def.doc)
    if members:
        members = members.group(2)
    else:
        return enum_def
    values = enum_values.findall(members)
    for i in range(len(values)):
        enum_def.values[values[i][1]] = i
    return enum_def

enum_class = re.compile("Enum Members")
def generate_members(_class, module="", cls=""):
    members = []
    for _,member in inspect.getmembers(_class):
        str_member = str(member)
        member_def = None
        if hasattr(member, "__objclass__") and member.__objclass__ == _class:
            if str_member.startswith("<attribute"):
                member_def = generate_attribute(member, module, cls)
            if str_member.startswith("<method"):
                member_def = generate_method(member, module, cls)
        elif str_member.startswith("<built-in method"): # Static/class methods
            to_ignore = [
                "__init_subclass__",
                "__new__",
                "__subclasshook__",
            ]
            if not member.__name__ in to_ignore:
                member_def = generate_method(member, module, cls, static=True)
        if member_def:
            members.append(member_def)

    return members

multi_linebreaks = re.compile(r'(\n){2,}')
def format_doc(doc, indent = 4*" ", base_indent = ""):
    if doc is None:
        return ""
    doc = multi_linebreaks.sub("\n", doc)
    doc = [
        base_indent + indent + doc.replace("\n", "\n"+indent+base_indent),
    ]
    return "\n".join(doc)

def get_type_from_docstring(doc_type_description):
    if doc_type_description.startswith("A tuple"):
        t = "tuple"
    elif doc_type_description.startswith("list of"):
        t = doc_type_description.replace("list of ", "List[").replace(":py:class:", "").replace("`", "") + "]"
        return t
    elif doc_type_description.startswith("Enum Member"):
        t = "int"
    else:
        t = doc_type_description.replace(":py:class:", "").replace("`", "")
    return generate_alias(t)

match_rtype = re.compile(r'(:rtype:)(\s*)?(:py:class:`)?(\s*)?(.*?)((\s|`)*?\n)')
def get_return_type(doc):
    rtype = match_rtype.search(doc)
    if(rtype is None):
        return ""
    rtype = rtype.group(5)
    return get_type_from_docstring(rtype)

test_field = re.compile(r'Field Value')
match_field_type = re.compile(r'(Type:)(.*?)(`)?(\s+\n)')
has_setter = re.compile(r'Setter Method')
def generate_attribute(attribute, module="", cls=""):
    prop_def = PropertyDefintion()
    prop_def.name = attribute.__name__
    doc = format_doc(attribute.__doc__, indent, indent) if attribute.__doc__ else ""
    prop_def.doc = doc
    if test_field.search(doc):
        rtype = get_type_from_docstring(match_field_type.search(doc).group(2))
    else:
        rtype = get_return_type(doc)
    prop_def.value_type = rtype
    prop_def.is_readonly = not bool(has_setter.search(doc))

    return prop_def

match_paramtypes = re.compile(r':type\s*(\w+):\s*(list of (:py:class:`.*?`|\w+)|:py:class:`.*?`|\w+)')
def generate_method(method, module, cls, static=False):
    method_def = MethodDefinition()
    method_def.name = method.__name__
    method_def.is_classmethod = static

    doc = method.__doc__
    if static and not doc:
        key = f"{module}.{cls}.{method.__name__}"
        doc = addin_docs[key] if key in addin_docs.keys() else ""
    doc = format_doc(doc, indent, indent)
    method_def.doc = doc

    method_def.returns = get_return_type(doc)

    parameters = match_paramtypes.findall(doc)
    for groups in parameters:
        method_def.params[groups[0]] = get_type_from_docstring(groups[1])
        parameters = ""
    if static:
        method_def.is_classmethod 
    return method_def

alias_pattern = re.compile(r'\.(?=.*\.)')
extract_qualified_name = re.compile(r'(?:\w+\s?\[)?((\w|\.)+)(?:\])?')
def generate_alias(type_hint, is_package = False):
    qualified_name = extract_qualified_name.search(type_hint).group(1)
    if is_package:
        alias = qualified_name.replace(".","_")
    else:
        alias = alias_pattern.sub("_", qualified_name)
    return type_hint.replace(qualified_name, alias)

def generate_imports (types, current_package):
    imports = set()
    relative_root = "." + len(current_package.split("."))*"."
    for t in types:
        if "." in t:
            if "_" in t and not "_Struct" in t:
                alias = t.split(".")[0]
                package, module = alias.replace("_",".").rsplit(".", 1)
                imports.add(f"from {relative_root}{package} import {module} as {alias}")
            else:
                imports.add(f"from {relative_root} import {t.split('.')[0]}")
    return list(imports)


def load_addin_docs(filepath):
    with open(filepath) as json_file:
        addin_docs = json.load(json_file)
        return addin_docs

# MAIN
print(f"Using NX found at '{os.environ['UGII_BASE_DIR']}' to create stubs.")
print(f"NX Version Info: {version_info}")
src_header = [
    "#!/usr/bin/env python",
    '"""',
    'Python stubs build for NXOpen.',
    f'Build with/for {version_info}',
    '"""',
    f"""__version__ = "{version_info.get_major_version()}" """,
    "",
]

modules = []
for module in modules_to_load:
    try:
        importlib.import_module(module)
        modules.append(module)
    except Exception as e:
         log.error(f"Cannot load '{module}': {e}")

addin_docs = load_addin_docs(addin_docs_path)

print(f"Writing files to '{out_dir}'.")
for module in modules:
    print(f"Creating stubs for module '{module}'")
    module_path = path.join(out_dir, os.sep.join(module.split(".")))
    if not path.exists(module_path):
        makedirs(module_path)

    py_module = sys.modules[module]
    init_imports = []

    for name, obj in inspect.getmembers(py_module, predicate=inspect.isclass):
        # print(obj.__name__)

        init_imports.append(obj.__name__)
        cls_def = generate_class(obj, module)

        imports = []
        imports += default_imports
        imports += generate_imports(cls_def.get_import_types(), module)
        
        cls_txt = src_header + imports + [""] + cls_def.render()

        with open(path.join(module_path, obj.__name__+extension), "w+", encoding="utf-8") as f:
            f.write("\n".join(cls_txt))

    with open(path.join(module_path, "__init__"+init_extension), "w+", encoding="utf-8") as f:
        f.write("\n".join(src_header))
        f.write("\n".join(map(lambda x: f"from .{x} import {x}", init_imports)))
