from typing import List, Dict, Set
import re

filter_type = re.compile(r'(?:\w+\s?\[)?((\w|\.)+)(?:\])?')
class Definition():
    def __init__(self) -> None:
        self.name: str

    def get_types(self) -> Set[str]:
        pass

    def get_import_types(self) -> Set[str]:
        types = self.get_types()
        types.discard("")
        types = set(map(lambda x: filter_type.search(x).group(1), types))
        return types


class ClassDefinition(Definition):
    def __init__(self) -> None:
        self.name: str
        self.doc: str
        self.imports: List[str] = []
        self.base_classes: List[str] = []
        self.members: List[MemberDefinition] = []
        
    def render(self, indent="    "):
        text = [
            f"class {self.name}({', '.join(self.base_classes)}):",
            indent+'"""',
            self.doc,
            indent+'"""'
            ""
        ]

        for member in self.members:
            text += member.render()
            text += [""]
        return text

    def get_types(self):
        types = set(self.base_classes)
        for member in self.members:
            types = types.union(member.get_types())
        return types

class EnumDefnition(ClassDefinition):
    def __init__(self) -> None:
        super().__init__()
        self.values: Dict[str, object] = {}

    def render(self, indent="    "):
        text = super().render()
        text += [""]
        for val in self.values.keys():
            text += [indent + f"{val} = {self.values[val]}"]
        return text

class MemberDefinition(Definition):
    def __init__(self) -> None:
        self.name: str
        self.member_type: str
        self.doc: str

class PropertyDefintion(MemberDefinition):
    def __init__(self) -> None:
        self.is_readonly: bool = True
        self.member_type = "Property"
        self.value_type: str = None

    def render(self, indent="    "):
        if not self.value_type is None:
            getter_hint = " -> " + self.value_type
            setter_hint = ": " + self.value_type
        else:
            getter_hint = setter_hint = ""

        text = [
            indent + "@property",
            indent + f"def {self.name}(self){getter_hint} :",
            2*indent+'"""',
            self.doc,
            2*indent+'"""',
            2*indent + "pass",
            ""
        ]

        if not self.is_readonly:
            text += [
                indent+f"@{self.name}.setter",
                indent+f"def {self.name} (self, value{setter_hint}):",
                indent+"    pass",
                ""
        ]
        return text

    def get_types(self):
        return set([self.value_type])

class MethodDefinition(MemberDefinition):
    def __init__(self) -> None:
        self.member_type: str = "Method"
        self.params: Dict[str, str] = {}
        self.returns: str = None
        self.is_classmethod: bool = False

    def render(self, indent="    "):
        parameters = []
        for param in self.params.keys():
            p_str = param + ("" if self.params[param] is None else ": " + self.params[param])
            parameters.append(p_str)
        rtype = " -> " + self.returns if self.returns else ""

        text = [
            indent+f"def {self.name}({'cls' if self.is_classmethod else 'self'}{', ' if self.params else ''}{', '.join(parameters)}){rtype} :",
            2*indent+'"""',
            self.doc,
            2*indent+'"""',
            2*indent+"pass",
        ]
        if self.is_classmethod:
            text = [indent+"@classmethod"] + text
        return text

    def get_types(self):
        return set([self.returns] + list(self.params.values()))