import os
from subprocess import Popen, PIPE

class NXVersionInfo():
    def __init__(self, ugii_base_dir = None) -> None:
        self.ugii_base_dir = ugii_base_dir or os.environ.get("UGII_BASE_DIR", None)
    
    def get_major_version(self):
        return self._env_print("v")

    def get_build_version(self):
        return self._env_print("n")

    def get_platform(self):
        return self._env_print("p")

    def _env_print(self, arg):
        if not self.ugii_base_dir:
            return None
        p = Popen(f"{os.path.join(self.ugii_base_dir, 'nxbin', 'env_print')} -{arg}", stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stdout.decode("utf-8").strip()

    def __str__(self):
        return f"NX {self.get_major_version()} ({self.get_build_version()}) - {self.get_platform()}"

if __name__ == "__main__":
    info = NXVersionInfo()
    print(info)

