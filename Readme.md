# NXOpen Python stubs

Stubs for NXOpen to (finally) provide usable typehints ([see also PEP 484](https://www.python.org/dev/peps/pep-0484/)) for python usable for most common IDEs e.g. VSCode, PyCharm. The stubs could be build for all NX versions >= 1847. 

## Install from Git

You can build/install the stubs from Git with pip. The stubs will be build from NX Open binaries for Python in a build step, so an installation of NX is required.

```` shell
pip install git+https://git.rwth-aachen.de/niemczik/nxopen-python-stubs.git
````

## NX Version

The build step makes use the UGII_BASE_DIR environment variable to locate the NX installation and further the NXOpen binaries for Python. So you can use this variable to generate the stubs for a specific version.

## Dependencies

The build process grabs the known modules/namespaces for each NX version from the official documentation. Therefore BeautifulSoup, Requests is utilized. If these are not installed, the build will fall back to a default list. So the dependencies are recommended but not necessary.

## Known Issues

1. No hints for tuples
2. NXOpen compiled modules may interfere with stubs (see Troubleshooting)

## Troubleshooting

The stubs may interfere with the compiled python modules of NX. This is easy to observe. If you start importing NXOpen and also see the names of other compiled modules like NXOpen_AME and get no autocompletion, the compiled modules may overwrite the stubs. The current solution is to add the binaries only for running/debugging the script to the PYTHONPATH.
